// https://www.indeed.com/viewjob?jk=30e6efd9f6b954a1
// go run web-scraper.go https://www.indeed.com/viewjob?jk=30e6efd9f6b954a1 https://www.indeed.com/viewjob?jk=39b5df93d35d3044

package main

import (
	"fmt"
	"golang.org/x/net/html"
	"net/http"
	"os"
	"encoding/json"
	"log"
	"io/ioutil"
	//"strings"
)

type response struct {
	Title string `json:"title"`
	Location string `json:"location"`
	Company string `json:"company"`
	Url string `json:"url"`
}

// Helper function to pull the attribute key from a Token
func getAttr(t html.Token, key string) (ok bool, href string) {
	// Iterate over all of the Token's attributes until we find an "href"
	for _, a := range t.Attr {
		if a.Key == key {
			href = a.Val
			ok = true
		}
	}
	
	// "bare" return will return the variables (ok, href) as defined in
	// the function definition
	return
}

// Extract all needed fields from the page
func crawl(url string, ch chan response, chFinished chan bool) {
	resp, err := http.Get(url)

	defer func() {
		// Notify that we're done after this function
		chFinished <- true
	}()

	if err != nil {
		fmt.Println("ERROR: Failed to crawl \"" + url + "\"")
		return
	}

	b := resp.Body
	defer b.Close() // close Body when the function returns

	z := html.NewTokenizer(b)

	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return
		case tt == html.StartTagToken:
			t := z.Token()

			// Check if the token is an <span> tag
			isSpan := t.Data == "span"
			if !isSpan {
				continue
			}

			// Extract the attr value, if there is one
			ok, jobtitle := getAttr(t, "data-indeed-apply-jobtitle")
			if !ok {
				continue
			}
			//ch <- jobtitle

			ok, joblocation := getAttr(t, "data-indeed-apply-joblocation")
			if !ok {
				continue
			}
			//ch <- joblocation

			ok, companyname := getAttr(t, "data-indeed-apply-jobcompanyname")
			if !ok {
				continue
			}
			//ch <- companyname

			ok, joburl := getAttr(t, "data-indeed-apply-joburl")
			if !ok {
				continue
			}
			//ch <- joburl
			res := response{}
			res.Title = jobtitle
			res.Location = joblocation
			res.Company = companyname
			res.Url = joburl
			ch <- res

			// Make sure the url begines in http**
			// hasProto := strings.Index(url, "http") == 0
			// if hasProto {
			// 	ch <- url
			// }
		}
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	foundUrls := make(map[response]bool)
	//seedUrls := os.Args[1:]

	// read the body of the request, expecting a JSON object of strings each containing a URL to scrape
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
    panic(err)
	}

	fmt.Printf("%s", b)

	// need to convert "b" which is json text into an array of strings which each contain a URL to scrape
	//dataJson := `["1","2","3"]`
	var seedUrls []string
	_ = json.Unmarshal([]byte(b), &seedUrls)
	// log.Printf("Unmarshaled: %v", seedUrls)
	// for idx, val := range seedUrls {
	// 	log.Printf("printing each seed url: %d %s", idx, val)
	// }

	// Channels
	chUrls := make(chan response)
	chFinished := make(chan bool) 

	// Kick off the crawl process (concurrently)
	for _, url := range seedUrls {
		go crawl(url, chUrls, chFinished)
	}

	// Subscribe to both channels
	for c := 0; c < len(seedUrls); {
		select {
		case url := <-chUrls:
			foundUrls[url] = true
		case <-chFinished:
			c++
		}
	}

	// We're done! Print the results...

	fmt.Println("\nFound", len(foundUrls), "unique urls:\n")
	output := "["

	len := len(foundUrls)
	index := 0
	for url, _ := range foundUrls {
		fmt.Println(" - " + url.Title)
		fmt.Println(" - " + url.Location)
		fmt.Println(" - " + url.Company)
		fmt.Println(" - " + url.Url)

		
		b, err := json.Marshal(url)
		output += string(b);
		//fmt.Fprintf(w, "Hi there, I love %s!", "["+string(b)+"]")
		// var out response
		// err = json.Unmarshal(b, &out)
		//fmt.Println(err)
		if err==nil {
			fmt.Println("--------------json start")
			fmt.Println(string(b[:]))
			fmt.Println("--------------json end")
			fmt.Println("======================")
		}
		index++

		if index<=len-1 {
			output += ","
		}
		
	}

	output += "]"

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", output)
	close(chUrls)

	
}

func start_scraper() {
	foundUrls := make(map[response]bool)
	seedUrls := os.Args[1:]

	// Channels
	chUrls := make(chan response)
	chFinished := make(chan bool) 

	// Kick off the crawl process (concurrently)
	for _, url := range seedUrls {
		go crawl(url, chUrls, chFinished)
	}

	// Subscribe to both channels
	for c := 0; c < len(seedUrls); {
		select {
		case url := <-chUrls:
			foundUrls[url] = true
		case <-chFinished:
			c++
		}
	}

	// We're done! Print the results...

	fmt.Println("\nFound", len(foundUrls), "unique urls:\n")

	for url, _ := range foundUrls {
		fmt.Println(" - " + url.Title)
		fmt.Println(" - " + url.Location)
		fmt.Println(" - " + url.Company)
		fmt.Println(" - " + url.Url)

		
		b, err := json.Marshal(url)
		// var out response
		// err = json.Unmarshal(b, &out)
		//fmt.Println(err)
		if err==nil {
			fmt.Println("--------------json start")
			fmt.Println("["+string(b[:])+"]")
			fmt.Println("--------------json end")
			fmt.Println("======================")
		}
		
	}

	close(chUrls)
}

func main() {

	http.HandleFunc("/", handler)
  log.Fatal(http.ListenAndServe(":8080", nil))

	
}